\babel@toc {greek}{}
\contentsline {section}{Εισαγωγή}{3}{Doc-Start}%
\contentsline {subsection}{Τεχνικές Λεπτομέρειες}{3}{section*.1}%
\contentsline {subsubsection}{Συγγραφή}{3}{section*.1}%
\contentsline {subsubsection}{Προγραμματισμός}{3}{section*.1}%
\contentsline {subsection}{Εξηγήσεις για τον φάκελο παραδοτέου}{3}{section*.1}%
\contentsline {subsection}{Αποθετήριο της εργασίας}{4}{section*.1}%
\contentsline {section}{\numberline {1}Αναφορά στην Εργασία 1}{5}{section.1}%
\contentsline {subsection}{\numberline {1.1}Ερώτηση 0.5 - Σύγκριση μεθόδων tokenization}{5}{subsection.1.1}%
\contentsline {subsubsection}{\numberline {1.1.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{5}{subsubsection.1.1.1}%
\contentsline {subsubsection}{\numberline {1.1.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{6}{subsubsection.1.1.2}%
\contentsline {paragraph}{Ανάλυση:}{6}{subsubsection.1.1.2}%
\contentsline {paragraph}{Συμπεράσματα:}{6}{subsubsection.1.1.2}%
\contentsline {section}{\numberline {2}Αρχικές μέθοδοι σύγκρισης}{7}{section.2}%
\contentsline {subsection}{\numberline {2.1}Ερώτηση 1 - Δημιουργία πίνακα συμπτώσεων}{7}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{7}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{11}{subsubsection.2.1.2}%
\contentsline {paragraph}{Ανάλυση:}{11}{subsubsection.2.1.2}%
\contentsline {paragraph}{Συμπεράσματα:}{11}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Ερώτηση 2 - Μορφοποίηση πινάκων με το Pandas}{12}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{12}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{16}{subsubsection.2.2.2}%
\contentsline {paragraph}{Ανάλυση:}{16}{subsubsection.2.2.2}%
\contentsline {paragraph}{Συμπεράσματα:}{17}{subsubsection.2.2.2}%
\contentsline {section}{\numberline {3}Πιο εξελιγμένες μέθοδοι σύγκρισης}{17}{section.3}%
\contentsline {subsection}{\numberline {3.1}Ερώτηση 3α - Σύγκριση Προτάσεων με το Pandas}{17}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{17}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{18}{subsubsection.3.1.2}%
\contentsline {paragraph}{Ανάλυση:}{18}{subsubsection.3.1.2}%
\contentsline {paragraph}{Συμπεράσματα:}{19}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Ερώτηση 3β - Σύγκριση βιβλίων με το Pandas}{19}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{19}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{20}{subsubsection.3.2.2}%
\contentsline {paragraph}{Ανάλυση:}{20}{subsubsection.3.2.2}%
\contentsline {paragraph}{Συμπεράσματα:}{20}{subsubsection.3.2.2}%
\contentsline {subsection}{\numberline {3.3}Ερώτηση 4 - Συχνά εμφανιζόμενες λέξεις}{21}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{21}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{25}{subsubsection.3.3.2}%
\contentsline {paragraph}{Ανάλυση:}{25}{subsubsection.3.3.2}%
\contentsline {paragraph}{Συμπεράσματα:}{26}{subsubsection.3.3.2}%
\contentsline {section}{\numberline {4}Ακόμα πιο ανεπτυγμένες μέθοδοι ανάλυσης}{26}{section.4}%
\contentsline {subsection}{\numberline {4.1}Ερώτηση 5 - Διαφορά συνημίτονων 2 προτάσεων}{26}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{27}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{28}{subsubsection.4.1.2}%
\contentsline {paragraph}{Ανάλυση:}{28}{subsubsection.4.1.2}%
\contentsline {paragraph}{Συμπεράσματα:}{28}{subsubsection.4.1.2}%
\contentsline {subsection}{\numberline {4.2}Ερώτηση 6 - Διαφορά συνημίτονων 2 βιβλίων}{29}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{29}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{29}{subsubsection.4.2.2}%
\contentsline {paragraph}{Ανάλυση:}{29}{subsubsection.4.2.2}%
\contentsline {paragraph}{Συμπεράσματα:}{29}{subsubsection.4.2.2}%
\contentsline {subsection}{\numberline {4.3}Ερώτηση 7 - Σύγκριση με ομοιότητα TF-IDF}{30}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{30}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{31}{subsubsection.4.3.2}%
\contentsline {paragraph}{Ανάλυση:}{31}{subsubsection.4.3.2}%
\contentsline {paragraph}{Συμπεράσματα:}{31}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Δυό πιθανά ερωτήματα}{32}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}\begingroup \normalfont \includegraphics [height=12pt]{code.png}\endgroup {} Κώδικας και αποτελέσματα}{32}{subsubsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.5}\begingroup \normalfont \includegraphics [height=12pt]{info.png}\endgroup {} Παρατηρήσεις}{33}{subsubsection.4.3.5}%
\contentsline {paragraph}{Ανάλυση:}{33}{subsubsection.4.3.5}%
\contentsline {paragraph}{Συμπεράσματα:}{34}{subsubsection.4.3.5}%
\contentsline {section}{Αναφορές}{35}{subsubsection.4.3.5}%
