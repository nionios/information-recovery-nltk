# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to compare two differently tokenized coincidence tables
#              but displayed with the help of the pandas library this time.
# NOTE: Part of Exercise 2
import nltk
import numpy as np
import pandas as pd
from nltk.tokenize import word_tokenize

def makeTable(token_sequence):
    vocab = sorted(set(token_sequence))
    num_tokens = len(token_sequence)
    vocab_size = len(vocab)
    onehot_vectors = np.zeros((num_tokens, vocab_size), int)
    for i, word in enumerate(token_sequence):
        onehot_vectors[i, vocab.index(word)] = 1
    print(' '.join(vocab))
    #Print the data with pandas to prettify it!
    print(pd.DataFrame(onehot_vectors, columns=vocab))

def question_2():
    sentence1 = "Thomas Jefferson began building Monticello at the age of 26."
    sentence2 = """In 1972, Kernighan described memory management in strings
                 using 'hello' and 'world', in the programming language B,
                 which became the iconic example we know today."""
    tokens1_split=sentence1.split()
    tokens1_nltk=word_tokenize(sentence1)
    tokens2_split=sentence2.split()
    tokens2_nltk=word_tokenize(sentence2)
    print("Our first sentence tokenized with split():")
    makeTable(tokens1_split)
    print("Our first sentence tokenized with NTLK's word_tokenize():")
    makeTable(tokens1_nltk)
    print("Our second sentence tokenized with split():")
    makeTable(tokens2_split)
    print("Our second sentence tokenized with NTLK's word_tokenize():")
    makeTable(tokens2_nltk)


def main():
    question_2()

if __name__ == "__main__":
    main()
