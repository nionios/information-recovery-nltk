# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to calculate the TF-IDF similarity of 2 books
# NOTE: Part of Exercise 7
import math
from nltk.book import text4, text7
from sklearn.feature_extraction.text import TfidfVectorizer

def cosine_sim(vec1, vec2):
     dot_prod = 0
     for i, v in enumerate(vec1):
        dot_prod += v * vec2[i]
     mag_1 = math.sqrt(sum([x**2 for x in vec1]))
     mag_2 = math.sqrt(sum([x**2 for x in vec2]))
     return dot_prod / (mag_1 * mag_2)

def transformer(docs):
    corpus = docs
    vectorizer = TfidfVectorizer(min_df=1)
    model = vectorizer.fit_transform(corpus)
    final_vec = model.todense().round(3)
    return final_vec

# This function takes only raw text and not tokens.
def question_7():
    # Creating the raw text from the tokenized variables
    space = " "
    text4_raw = space.join(text4[0:50])
    text7_raw = space.join(text7[0:50])
    docs = [text4_raw,text7_raw]
    sent_vec = transformer(docs)
    # Conctruct the query vectors
    query_list_1 = ["Some thing about House of Representatives",
                   "This is totally irrelavant"]
    query_vec_1 = transformer(query_list_1)
    query_list_2 = ["",
                   "Irrelavant"]
    query_vec_2 = transformer(query_list_2)
    # Feed the query vectors and the sentence vectors to cosine_sim
    print(cosine_sim(query_vec_1[0],sent_vec[0]))
    print(cosine_sim(query_vec_1[1],sent_vec[0]))
    print(cosine_sim(sent_vec[1],sent_vec[1]))
    print(cosine_sim(query_vec_2[1],sent_vec[1]))

def main():
    question_7()

if __name__ == "__main__":
    main()
