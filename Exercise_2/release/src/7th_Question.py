# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to calculate the TF-IDF similarity of 2 books
# NOTE: Part of Exercise 7
import math
from nltk.book import text4, text7
from sklearn.feature_extraction.text import TfidfVectorizer

def cosine_sim(vec1, vec2):
     dot_prod = 0
     for i, v in enumerate(vec1):
        dot_prod += v * vec2[i]
     mag_1 = math.sqrt(sum([x**2 for x in vec1]))
     mag_2 = math.sqrt(sum([x**2 for x in vec2]))
     return dot_prod / (mag_1 * mag_2)

# This function takes only raw text and not tokens.
def question_7(docs):
    corpus = docs
    vectorizer = TfidfVectorizer(min_df=1)
    model = vectorizer.fit_transform(corpus)
    fin = model.todense().round(3)
    print(f"The TF-IDF similarity is: {cosine_sim(fin[0],fin[1])}")

def main():
    # Creating the raw text from the tokenized variables
    space = " "
    text4_raw = space.join(text4[0:50])
    text7_raw = space.join(text7[0:50])
    docs = [text4_raw,text7_raw]
    question_7(docs)

if __name__ == "__main__":
    main()
