# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-03
# Description: A script to compare two tokenization methods
# NOTE: Part of Exercise 0.5 (Pre-Exercise 1)
import nltk
from nltk.tokenize import word_tokenize

def question_05():
    sentence1 = "Thomas Jefferson began building Monticello at the age of 26."
    sentence2 = """In 1972, Kernighan described memory management in strings
                 using 'hello' and 'world', in the programming language B,
                 which became the iconic example we know today."""
    tokens1_split=sentence1.split()
    tokens1_nltk=word_tokenize(sentence1)
    tokens2_split=sentence2.split()
    tokens2_nltk=word_tokenize(sentence2)
    print(f"With split():\n The first sentence tokenized is: {tokens1_split}")
    print(f" The second sentence tokenized is: {tokens2_split}")
    print(f"With word_tokenize():\n The first sentence tokenized is: {tokens1_nltk}")
    print(f" The second sentence tokenized is: {tokens2_nltk}")

def main():
    question_05()

if __name__ == "__main__":
    main()
