# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to test some text analyzation helper techniques
# NOTE: Part of Exercise 5
import nltk
from nltk import FreqDist
from nltk.tokenize import word_tokenize

def question_5():
    raw="""This IS a sEntEnCE thaT Has bEen ThROUgH A raNdOMcaSe CONVertEr.
         tHiS iS ANotHER SEntenCE, CoNTAInInG aNd REpEaTINg more wOrds. thIS
         tExT COntains 4 SeNTencEs, sOME Words ARe ObViOUSlY rEPEatEd, it iS
         OBVIOuS."""
    print(f"That text is: '{raw}'")
    tokens=word_tokenize(raw)
    print(f"That text tokenized is: {tokens}")
    fdist=FreqDist(tokens)
    fdist.plot(20)

    #Normalizing tokens
    tokens_normalized=[x.lower() for x in tokens]
    print(f"That text normalized and tokenized is: {tokens_normalized}")
    fdist_normalized=FreqDist(tokens_normalized)
    fdist_normalized.plot(20)

    porter = nltk.PorterStemmer()
    tokens_stemmed=[porter.stem(t) for t in tokens]
    print(f"That text normalized,tokenized and stemmed is: {tokens_stemmed}")
    fdist_stemmed=FreqDist(tokens_stemmed)
    fdist_stemmed.plot(20)

def main():
    question_5()

if __name__ == "__main__":
    main()
