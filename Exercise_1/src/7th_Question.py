# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to compare the results of two different tokenization
#              methods.
# NOTE: Part of Exercise 7
import nltk

def question_7():
    nltk.download('stopwords')
    english_stopwords = nltk.corpus.stopwords.words('english')
    greek_stopwords = nltk.corpus.stopwords.words('greek')
    print(f"""*** This is part of question 7 ***
    The stopwords in English are: {len(english_stopwords)}
    The stopwords in Greek are: {len(greek_stopwords)}""")

def main():
    question_7()

if __name__ == "__main__":
    main()
