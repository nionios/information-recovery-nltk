# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to compare the results of two different tokenization
#              methods.
# NOTE: Part of Exercise 8
import nltk
import string
import nltk.tokenize
from nltk.probability import FreqDist

from nltk.util import clean_html

def openFile(filename):
    with open('./sample_texts/' + filename , 'r') as file:
            raw = file.read()
            file.close()
    return raw

#Providing the language and the name of the file to clean and analyse
def question_8(lang,filename):
    raw=openFile(filename)
    #Tokenize with the nltk function
    tokens=nltk.word_tokenize(raw)
    if lang == "english":
        stopwords = nltk.corpus.stopwords.words('english')
    elif lang == "greek":
        stopwords = nltk.corpus.stopwords.words('greek')
    else:
        print(f"* Error: Language {lang} not supported.")
        return 1
    cleaned_tokens = []
    for token in tokens:
        if token not in string.punctuation and stopwords:
            cleaned_tokens.append(token)
    #Plot the findings
    fdist=FreqDist(cleaned_tokens)
    fdist.plot(50)

def main():
    print(f"""*** This is part of question 8 ***""")
    question_8("english","Sense-and-Sensibility-200-words.txt")
    question_8("greek","What-is-Copyleft-Greek.txt")

if __name__ == "__main__":
    main()
