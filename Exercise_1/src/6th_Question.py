# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to compare the results of two different tokenization
#              methods.
# NOTE: Part of Exercise 6
import nltk
from nltk.probability import FreqDist
import nltk.tokenize

def openFile(filepath):
    with open('./sample_texts/' + filepath , 'r') as file:
            raw = file.read()
            file.close()
    return raw

def makeGraphSplit(filepath):
    raw=openFile(filepath)
    #Tokenize "traditionally" with python's string manipulation split() function
    tokens=raw.split()
    print(f"* The text tokenized with split() is: {tokens}")
    fdist=FreqDist(tokens)
    fdist.plot(50)

def makeGraphTokenize(filepath):
    raw=openFile(filepath)
    #Tokenize with the nltk function this time
    tokens=nltk.word_tokenize(raw)
    print(f"* The text tokenized with ntlk.word_tokenize() is: {tokens}")
    fdist=FreqDist(tokens)
    fdist.plot(50)

def question_6():
    makeGraphSplit("What-is-Copyleft-Greek.txt")
    makeGraphSplit("Sense-and-Sensibility-200-words.txt")
    makeGraphTokenize("What-is-Copyleft-Greek.txt")
    makeGraphTokenize("Sense-and-Sensibility-200-words.txt")

def main():
    question_6()

if __name__ == "__main__":
    main()
