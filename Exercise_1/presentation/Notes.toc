\babel@toc {greek}{}
\contentsline {section}{Εισαγωγή}{3}{Doc-Start}%
\contentsline {subsection}{Τεχνικές Λεπτομέρειες}{3}{section*.1}%
\contentsline {subsubsection}{Συγγραφή}{3}{section*.1}%
\contentsline {subsubsection}{Προγραμματισμός}{3}{section*.1}%
\contentsline {subsection}{Εξηγήσεις για τον φάκελο παραδοτέου}{3}{section*.1}%
\contentsline {subsection}{Αποθετήριο της εργασίας}{3}{section*.1}%
\contentsline {section}{\numberline {1}Απλά Στατιστικά}{4}{section.1}%
\contentsline {subsection}{\numberline {1.1}Ερώτηση 1 - Ποσοστιαία χρήση λέξεων}{4}{subsection.1.1}%
\contentsline {subsubsection}{\numberline {1.1.1}Αποτελέσματα κώδικα}{4}{subsubsection.1.1.1}%
\contentsline {subsubsection}{\numberline {1.1.2}Παρατηρήσεις}{5}{subsubsection.1.1.2}%
\contentsline {paragraph}{Ανάλυση:}{5}{subsubsection.1.1.2}%
\contentsline {paragraph}{Συμπεράσματα:}{5}{subsubsection.1.1.2}%
\contentsline {subsection}{\numberline {1.2}Ερώτηση 2 - Γράφημα χρήσης λέξεων Moby Dick}{6}{subsection.1.2}%
\contentsline {subsubsection}{\numberline {1.2.1}Παρατηρήσεις}{7}{subsubsection.1.2.1}%
\contentsline {paragraph}{Ανάλυση:}{7}{subsubsection.1.2.1}%
\contentsline {paragraph}{Συμπεράσματα:}{7}{subsubsection.1.2.1}%
\contentsline {subsection}{\numberline {1.3}Ερώτηση 3 - Γράφημα χρήσης λέξεων Monty Python}{8}{subsection.1.3}%
\contentsline {subsubsection}{\numberline {1.3.1}Παρατηρήσεις}{9}{subsubsection.1.3.1}%
\contentsline {paragraph}{Ανάλυση:}{9}{subsubsection.1.3.1}%
\contentsline {paragraph}{Συμπεράσματα:}{9}{subsubsection.1.3.1}%
\contentsline {section}{\numberline {2}Κανονικοποίηση Κειμένου}{10}{section.2}%
\contentsline {subsection}{\numberline {2.1}Ερώτηση 4 - Απλή κανονικοποίηση}{10}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Παρατηρήσεις}{10}{subsubsection.2.1.1}%
\contentsline {paragraph}{Ανάλυση:}{10}{subsubsection.2.1.1}%
\contentsline {paragraph}{Συμπεράσματα:}{10}{subsubsection.2.1.1}%
\contentsline {subsection}{\numberline {2.2}Ερώτηση 5 - Άλλες τεχνικές κανονικοποίησης}{11}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Παρατηρήσεις}{12}{subsubsection.2.2.1}%
\contentsline {paragraph}{Ανάλυση:}{12}{subsubsection.2.2.1}%
\contentsline {paragraph}{Συμπεράσματα:}{14}{subsubsection.2.2.1}%
\contentsline {section}{\numberline {3}Tokenization}{15}{section.3}%
\contentsline {subsection}{\numberline {3.1}Ερώτηση 6 - Μέθοδοι Tokenization}{15}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Παρατηρήσεις}{16}{subsubsection.3.1.1}%
\contentsline {paragraph}{Ανάλυση:}{16}{subsubsection.3.1.1}%
\contentsline {paragraph}{Συμπεράσματα:}{19}{subsubsection.3.1.1}%
\contentsline {section}{\numberline {4}Αφαίρεση σημείων στίξης και προθημάτων (stop words)}{20}{section.4}%
\contentsline {subsection}{\numberline {4.1}Ερώτηση 7 - Αναφορά στα Stopwords}{20}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Παρατηρήσεις}{20}{subsubsection.4.1.1}%
\contentsline {subsection}{\numberline {4.2}Ερώτηση 8 - Πλήρης καθαρισμός κειμένου}{21}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Παρατηρήσεις}{21}{subsubsection.4.2.1}%
\contentsline {paragraph}{Ανάλυση:}{21}{subsubsection.4.2.1}%
\contentsline {paragraph}{Συμπέρασματα:}{23}{subsubsection.4.2.1}%
\contentsline {subsection}{\numberline {4.3}Ερώτηση 9 - Σύγκριση Αρχικών/Τελικών Αποτελεσμάτων}{24}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Τελική Ανάλυση Αποτελεσμάτων}{24}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Συμπεράσματα εργασίας}{26}{subsubsection.4.3.2}%
\contentsline {section}{Αναφορές}{27}{subsubsection.4.3.2}%
