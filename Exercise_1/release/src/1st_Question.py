# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to count and make some statistics with words
#              from books in the nltk module.
# NOTE: Part of Exercise 1
import nltk
from nltk.book import text6, text5

# Wrote a function to check number of word occurances and percentage for
# convinient analysing. Results are returned in a dictionary form.
def analysis(word,text_len,text):
    res_dict = dict()
    res_dict["count"] = text.count(word)
    res_dict["perc"]  = ( 100*res_dict.get("count") ) / text_len
    return res_dict

def question_1():

    # 1ai & 1aii
    ## Getting word number and distinct word number from :
    ## text6 == Book "Monty Python and the Holy Grail"
    ## text5 == Large text body of online chats/posts "Chat Corpus"
    text5_len = len(text5)
    text6_len = len(text6)
    words_in_text6 = len(set(text6))
    words_in_text5 = len(set(text5))
    ## Counting given words and calculating percentages, saving them in dicts
    ## Text 6 (Monty Python)
    LAUNCELOT = analysis("LAUNCELOT",text6_len,text6)
    ## Text 5 (Chat Corpus)
    lol = analysis("lol",text5_len,text5)
    omg = analysis("omg",text5_len,text5)
    OMG = analysis("OMG",text5_len,text5)
    # 1b
    ## Same thing with 3 other words in each text
    ## Text 6
    grail = analysis("grail",text6_len,text6)
    silly = analysis("silly",text6_len,text6)
    business = analysis("business",text6_len,text6)
    ## Text 5
    love = analysis("love",text5_len,text5)
    hate = analysis("hate",text5_len,text5)
    u = analysis("u",text5_len,text5)


    print("\n"
    "┌──Question 1 - Simple statistics\n" +
    "│ ┌─Part a\n"
    "│ │ ┌─Subpart i\n" +
    "│ │ │ The distinct words in 'Monty Python and the Holy Grail' are "
    + str(words_in_text6) + ".\n" +
    "│ │ │ 'LAUNCELOT' appears " + str(LAUNCELOT.get("count")) +
    " times, it is " + str(LAUNCELOT.get("perc"))  + "% of the text.\n" +
    "│ │ └─\n"
    "│ │ ┌─Subpart ii\n" +
    "│ │ │ The number of distinct words in 'Chat Corpus' is "
    + str(words_in_text5) + ".\n" +
    "│ │ │ 'lol' appears " + str(lol.get("count")) +
    " times, it is " + str(lol.get("perc")) + "% of the text.\n"+
    "│ │ │ 'omg' appears " + str(omg.get("count")) +
    " times, it is " + str(omg.get("perc")) + "% of the text.\n"+
    "│ │ │ 'OMG' appears " + str(OMG.get("count")) +
    " times, it is " + str(OMG.get("perc")) + "% of the text.\n"+
    "│ │ └─\n"
    "│ └─\n"
    "│ ┌─Part b\n"
    "│ ├In Monty Python and the Holy Grail:\n" +
    "│ │ 'grail' appears " + str(grail.get("count")) +
    " times, it is " + str(grail.get("perc")) + "% of the text.\n"+
    "│ │ 'silly' appears " + str(silly.get("count")) +
    " times, it is " + str(silly.get("perc")) + "% of the text.\n"+
    "│ │ 'business' appears " + str(business.get("count")) +
    " times, it is " + str(business.get("perc")) + "% of the text.\n"+
    "│ ├In Chat Corpus:\n" +
    "│ │ 'love' appears " + str(love.get("count")) +
    " times, it is " + str(love.get("perc")) + "% of the text.\n"+
    "│ │ 'hate' appears " + str(hate.get("count")) +
    " times, it is " + str(hate.get("perc")) + "% of the text.\n"+
    "│ │ 'u' appears " + str(u.get("count")) +
    " times, it is " + str(u.get("perc")) + "% of the text.\n"
    "│ └─\n"
    "└─\n"
    )

def main():
    question_1()

if __name__ == "__main__":
    main()
