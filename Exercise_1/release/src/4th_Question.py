# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Description: A program to normalize the first sentence of the Moby Dick book.
# NOTE: Part of Exercise 4
import nltk
from nltk.book import text1,sent1

def question_4():
    print(f"The first sentence of Moby Dick is: {sent1}")
    tokens1=sent1
    normalized_sent1=[x.lower() for x in tokens1]
    print(f"That sentence normalized is: {normalized_sent1}")

def main():
    question_4()

if __name__ == "__main__":
    main()
