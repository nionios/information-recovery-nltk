# Information Recovery

This project uses the [NTLK](https://www.nltk.org/) library in the
[Python](https://www.python.org/) programming languge.

The goal of this repository is to study and understand different methods
and concepts around Information Recovery as a part of Computer Science.

The nltk library is licensed under the [Apache License
2.0](https://www.apache.org/licenses/LICENSE-2.0).

This study is licensed under the
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

These two licenses are
[compatible](https://www.apache.org/licenses/GPL-compatibility.html).

Other files like exercise descriptions are subject to the copyright of their
respective owners and/or copyright of [UniWa, Department of Information and
Computer Engineering](http://www.ice.uniwa.gr).
