# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-18
# Description: A script to compare the Levenstein distance of more words
# NOTE: Part of Exercise A, part 2
from nltk.metrics import edit_distance

def comparer(word_1, word_2):
    dist = edit_distance(word_1,word_2)
    print(f"'{word_1}' and '{word_2}' have a Levenstein distance of {dist}.")

def exercise_A_part_2():
    comparer("Τάσος","Νάσος")
    comparer("Διαφορά","Φιαδωρά")
    comparer("Λάθος","Λκσάδφιεθος")
    comparer("Completely Right","Completely Right")
    comparer("Utterly wrong","zzz")

def main():
    exercise_A_part_2()

if __name__ == "__main__":
    main()
