# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-18
# Description: A script to compare the Levenstein distance of two words
# NOTE: Part of Exercise A, part 1
from nltk.metrics import edit_distance

def exercise_A_part_1():
    word_1 = "Thomas Jefferson"
    word_2 = "Spomas Befferpon"
    dist = edit_distance(word_1,word_2)
    print(f"'{word_1}' and '{word_2}' have a Levenstein distance of {dist}.")

def main():
    exercise_A_part_1()

if __name__ == "__main__":
    main()
