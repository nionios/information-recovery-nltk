# Author: Dionisis Nikolopoulos
# Student Code: 18390126
# License: GPLv3
# Date Written: 2021-12-18
# Description: A script to fix mistyped words on the terminal.
# NOTE: Part of Exercise B
import nltk
from nltk.metrics.distance import jaccard_distance
from nltk.corpus import words
from nltk.util import ngrams

def corrector(input_words):
    print("These are the corrected words you typed:")
    # Need the correct words to swap with the wrong ones
    nltk.download('words')
    correct_words = words.words()
    # Find correct spellings based on jaccard distance
    for word in input_words:
        temp = [(jaccard_distance(set(ngrams(word, 2)),
                                  set(ngrams(w, 2))),w)
                for w in correct_words if w[0]==word[0]]
        print(sorted(temp, key = lambda val:val[0])[0][1])

def exercise_A_part_2():
    words = []
    word_num = int(input("How many words do you need to type?\nAnswer -> "))
    i = 1
    # Get a number of words from the user
    print("Please type the words now separated with <Enter>:")
    while (i <= word_num):
        temp=input(f"* Word ({i}/{word_num}): ")
        words.append(temp)
        i+=1
    corrector(words)

def main():
    exercise_A_part_2()

if __name__ == "__main__":
    main()
